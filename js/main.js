$(document).ready(function(){
	generalScripts();
	var isMobile = $(window).width() > 640 ? false : true;
	if(isMobile) mobileScripts();
	else desktopScripts();
});
$(window).resize(function() {
	var gridSize = getGridSize();

	$('#carousel').data('flexslider').vars.maxItems = gridSize;
	$('#carousel').data('flexslider').update();
});
function generalScripts(){
	 initGallerySlider();
	 initHomeSlider();
	 initFancybox();
	 initShowContact();
}
function desktopScripts(){
	
}
function mobileScripts(){
	initMobileMenu();
}
function initHomeSlider(){
	$('.bxslider').bxSlider({
		pager:false
	});
}
function getGridSize() {
	return (window.innerWidth < 640) ? 1 : (window.innerWidth < 960) ? 2 : 4;
}
function initGallerySlider(){
	 $('#carousel').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
	    itemWidth: 200,
	    maxItems: getGridSize(),
	    asNavFor: '#slider'
	  });
	 
	  $('#slider').flexslider({
	    animation: "slide",
	    controlNav: false,
	    directionNav: false,
	    animationLoop: false,
	    slideshow: false,
	    smoothHeight:true,
	    sync: "#carousel"
	  });
}
function initFancybox(){
	 $(".project-item .hover-overlay").fancybox({
	 	padding:0,
	 	closeBtn:false
	 });
	 $(".service-category-items a").fancybox({
	 	padding:0,
	 	closeBtn:false
	 });
}
function initShowContact(){
	var openBtn = $('.open-button');

	openBtn.each(function(){
		$(this).click(function(){
			$(this).siblings('.show-section').slideToggle();
		})
	})
}
function initMobileMenu(){
	var openBtn = $('.mobile-menu-btn')

	 openBtn.click(function(){
			$('.main-menu').slideToggle();
		})
}
 
 
